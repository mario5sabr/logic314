--Tugas SQL Day 02

create database DB_Entertainer;
use DB_Entertainer;
create table tblArtis(
id bigint primary key identity (1,1),
kd_artis varchar(10) not null,
nm_artis varchar(100) not null,
jk varchar(10) not null,
bayaran int not null,
award int not null,
negara varchar (3) not null )

select * from tblArtis

insert into tblArtis(kd_artis,nm_artis,jk,bayaran,award,negara) 
values ('A001','ROBERT DOWNEY JR','PRIA', 3000000000, 2, 'AS'),
		('A002', 'ANGELINA JOLIE', 'WANITA', 7000000000, 1, 'AS'),
		('A003', 'JACKIE CHAN', 'PRIA', 2000000000, 7, 'HK'),
		('A004', 'JOE TASLIM', 'PRIA', 3500000000, 1, 'ID'),
		('A005', 'CHELSEA ISLAN', 'WANITA', 3000000000, 0, 'ID')

alter table tblArtis alter column bayaran decimal (20)

create table tblFilm(
	id bigint primary key identity (1,1),
	kd_film varchar (10) not null,
	nm_film varchar (55) not null,
	genre varchar (55) not null,
	artis varchar (55) not null,
	produser varchar (55) not null,
	pendapatan bigint not null,
	nominasi int not null 
)
select * from tblFilm

insert into tblFilm(kd_film,nm_film,genre,artis,produser,pendapatan, nominasi) 
values ('F001','IRON MAN','G001', 'A001','PD01',2000000000, 3),
		('F002', 'IRON MAN 2', 'G001', 'A001','PD01',1800000000, 2),
		('F003', 'IRON MAN 3', 'G001', 'A001','PD01',1200000000, 0),
		('F004', 'AVENGER CIVIL WAR', 'G001', 'A001','PD01',2000000000, 1),
		('F005', 'SPIDERMAN HOME COMING', 'G001', 'A001','PD01',1300000000, 0),
		('F006', 'THE RAID', 'G001', 'A004','PD03',800000000, 5),
		('F007', 'FAST & FARIOUS', 'G001', 'A004','PD05',830000000, 2),
		('F008', 'HABIBIE DAN AINUN', 'G004', 'A005','PD03',670000000, 4),
		('F009', 'POLICE STORY', 'G001', 'A003','PD02',700000000, 3),
		('F010', 'POLICE STORY 2', 'G001', 'A003','PD02',710000000, 1),
		('F011', 'POLICE STORY 3', 'G001', 'A003','PD02',615000000, 0),
		('F012', 'RUSH HOUR', 'G003', 'A003','PD05',695000000, 2),
		('F013', 'KUNGFU PANDA', 'G003', 'A003','PD05',923000000, 5)

		delete from tblFilm where id <= 13

create table tblProduser(
	id bigint primary key identity (1,1),
	kd_produser varchar (50) not null,
	nm_produser varchar (50) not null,
	international varchar (50) not null
)

select * from tblProduser

insert into tblProduser(kd_produser, nm_produser, international
) values ('PD01', 'MARVEL', 'YA'), ('PD02', 'HONGKONG CINEMA', 'YA'),
('PD03', 'RAPI FILM', 'TIDAK'), ('PD04', 'PARKIT', 'TIDAK'),
('PD05', 'PARAMOUNT PICTURE', 'YA')

SELECT * FROM tblProduser

create table tblNegara(
	id bigint primary key identity (1,1),
	kd_negara varchar (100),
	nm_negara varchar (100)
)

insert into tblNegara(kd_negara, nm_negara) values (
	'AS', 'AMERIKA SERIKAT'
),
('HK', 'HONGKONG'),
('ID', 'INDONESIA'),
('IN', 'INDIA')

SELECT * FROM tblNegara

create table tblGenre (
	id bigint primary key identity (1,1),
	kd_genre varchar (50) not null,
	nm_genre varchar (50) not null
)

insert into tblGenre (kd_genre, nm_genre) values (
	'G001', 'ACTION'
),
('G002', 'HORROR'),
('G003', 'COMEDY'),
('G004', 'DRAMA'),
('G005', 'THRILLER'),
('G006', 'FICTION')

SELECT * FROM tblGenre

--1. Menampilkan jumlah pendapatan produser marvel secara keseluruhan
select produs.nm_produser, SUM(tblFilm.pendapatan) as total_pendapatan
from tblFilm
join tblProduser produs on tblFilm.produser =produs.kd_produser
where produs.nm_produser = 'MARVEL'
group by produs.nm_produser

--2. Menampilkan nama film dan nominasi yang tidak mendapatkan nominasi
select tblFilm.nm_film, tblFilm.nominasi
from tblFilm
where nominasi = 0

--3. Menampilkan nama film yang huruf depannya 'p'

select tblFilm.nm_film
from tblFilm
where nm_film like 'p%'

--4. Menampilkan nama film yang huruf terakhir 'y'
select tblFilm.nm_film
from tblFilm
where nm_film like '%y'

--5. Menampilkan nama film yang mengandung huruf 'd'
select tblFilm.nm_film
from tblFilm
where nm_film like '%d%'

--6. Menampilkan nama film dan artis
select tblFilm.nm_film from tblFilm
join tblArtis art on tblFilm.artis = art.nm_artis


