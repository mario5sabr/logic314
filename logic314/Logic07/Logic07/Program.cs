﻿using System;
using System.Collections.Generic;

namespace Logic07
{
    class Mamalia
    {
        public virtual void pindah()
        {
            Console.WriteLine("Kucing Lari...");

        }
    }
    class Kucing : Mamalia
    {

    }

    class Paus : Mamalia
    {
        public override void pindah()
        {
            Console.WriteLine("Paus Berenang...");
        }
        internal class Program
        {
            static readonly List<User> listUsers = new List<User>();
            static void Main(string[] args)
            {
                //bool ulangi = true;

                //while (ulangi) {
                //    insertUser();

                //}
                //listUser();
                //dateTime();


                //mobil();

                //overriding();
                //timeSpan();
                stringDateTime();
            }
            static void overriding()
            {
                Paus paus = new Paus();
                paus.pindah();

                Kucing kucing = new Kucing();
                kucing.pindah();

            }

            static void classInheritance()
            {
                TypeMobil typeMobil = new TypeMobil();
                typeMobil.kecepatan = 100;
                typeMobil.posisi = 50;
                typeMobil.posisi = 50;
                typeMobil.Civic();
            }

            static void mobil()
            {
                TypeMobil typeMobil = new TypeMobil();
                typeMobil.kecepatan = 100;
                typeMobil.posisi = 50;
                typeMobil.posisi = 50;
                typeMobil.Civic();
            }
            static void timeSpan()
            {
                Console.WriteLine("=== TIME SPAN ===");
                DateTime date1 = new DateTime(2023, 1, 1, 0, 0, 0);
                DateTime date2 = DateTime.Now;

                TimeSpan span = date2 - date1;

                Console.WriteLine($"Interval hari : {span.Days}");
                Console.WriteLine($"Total hari : {span.TotalDays}");
                Console.WriteLine($"Interval Jam : {span.Hours}");
                Console.WriteLine($"Total Jam : {span.TotalHours}");
                Console.WriteLine($"Total Menit : {span.TotalMinutes}");
                Console.WriteLine($"Total Detik : {span.TotalSeconds}");
            }
            static void stringDateTime()
            {
                Console.WriteLine("=== DATE TIME ===");
                Console.Write("Masukkan tanggal (dd/mm/yyyy) : ");
                string dateString = Console.ReadLine();

                DateTime tanggal = DateTime.Parse(dateString);

                Console.WriteLine($"Tanggal : {tanggal.Day}");
                Console.WriteLine($"Bulan : {tanggal.Month}");
                Console.WriteLine($"Tahun : {tanggal.Year}");
                Console.WriteLine($"DayOfWeek ke : {(int)tanggal.DayOfWeek}");
                Console.WriteLine($"Tanggal : {tanggal.DayOfWeek}");
            }
            static void dateTime()
            {
                DateTime dt1 = new DateTime();
                Console.WriteLine(dt1);

                DateTime dt2 = DateTime.Now;
                Console.WriteLine(dt2);

                DateTime dt3 = DateTime.Now.Date;
                Console.WriteLine(dt3);

                DateTime dt4 = DateTime.UtcNow;
                Console.WriteLine(dt4);

                DateTime dt5 = new DateTime(2023, 03, 09);
                Console.WriteLine(dt5);

                DateTime dt6 = new DateTime(2023, 03, 09, 10, 44, 00);
                Console.WriteLine(dt6);
            }
            static void insertUser()
            {
                Console.WriteLine("=== Insert User ===");
                Console.Write("Nama : ");
                string nama = Console.ReadLine();
                Console.Write("Umur : ");
                int umur = int.Parse(Console.ReadLine());
                Console.Write("Alamat : ");
                string alamat = Console.ReadLine();



                User user = new User();
                user.Nama = nama;
                user.Umur = umur;
                user.Alamat = alamat;


                listUsers.Add(user);
                for (int i = 0; i < listUsers.Count; i++)
                {
                    Console.WriteLine("Nama Saya " + listUsers[i].Nama + " dengan umur " + listUsers[i].Umur + ", " + listUsers[i].Nama + " berasal dari " + listUsers[i].Alamat);
                }
            }

            static void listUser()
            {
                Console.WriteLine("=== List User ===");
                List<User> listUser = new List<User>()
            {
                new User() { Nama = "Firdha", Umur = 24, Alamat = "Tangsel"},
                new User() { Nama = "Isni", Umur = 22, Alamat = "Cimahi"},
                new User() { Nama = "Asti", Umur = 23, Alamat = "Garut"},
                new User() { Nama = "Muafa", Umur = 22, Alamat = "Bogor"},
                new User() { Nama = "Toni", Umur = 24, Alamat = "Garut"}
            };
                for (int i = 0; i < listUser.Count; i++)
                {
                    Console.WriteLine("Nama Saya " + listUser[i].Nama + " dengan umur " + listUser[i].Umur + ", " + listUser[i].Nama + " berasal dari " + listUser[i].Alamat);
                }
            }
        }
    }
}
