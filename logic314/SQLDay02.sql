--SQL day 02

--cast
select cast(10 as decimal(18,4))
select cast('10' as int)
select cast('2023-03-16' as datetime) jam
select GETDATE(), GETUTCDATE()
select cast(month(getdate()) as varchar)
select month(getdate()) bulan, day(getdate()) hari, year(getdate()) tahun

--convert
select CONVERT(decimal(18,4), 10)
select CONVERT(int, '10')
select CONVERT(int, 10.65)
select CONVERT(DATETIME, '2023-03-16') jam

--dateadd
select DATEADD (year,2,GETDATE()), DATEADD(month,3,GETDATE()), DATEADD(DAY,5,GETDATE())

--datediff
select DATEDIFF(day,'2023-03-16', '2023-03-25') Jarakhari
select DATEDIFF(month,'2023-03-16', '2023-06-25') jarakBulan
select DATEDIFF(year,'2023-03-16', '2024-03-25') jarakTahun

--subQuery
insert into [dbo].[mahasiswa]
select nama,alamat,email,panjang from mahasiswa

--index
create index index_name
on mahasiswa (nama)

create index index_Adress_email
on mahasiswa(alamat,email)

select * from mahasiswa where nama = 'Toni'
select * from mahasiswa

--create unique index
create unique index uniqueindex_alamat
on mahasiswa(alamat)

--drop index
drop index index_alamat_email on mahasiswa --sama dengan unique index

--add primary key
alter table mahasiswa add constraint pk_alamat primary key(alamat)

--drop primary key
alter table mahasiswa drop constraint PK__mahasiswa__

delete from mahasiswa where id >= 5
select * from mahasiswa

update mahasiswa set panjang = 48 where id =1
update mahasiswa set panjang =86 where id =2
update mahasiswa set panjang =117 where id =4
update mahasiswa set panjang =100 where id =3