﻿using System;

namespace Logic04
{
    internal class Program
    {
        static void Main(string[] args)
        {
            replaceString();

            Console.ReadKey();
        }
        static void replaceString() {
            Console.WriteLine("=== Replace String ===");
            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();
            Console.Write("Dari kata: ");
            string kataLama = Console.ReadLine();

            Console.Write("Replace menjadi kata : ");
            string kataBaru = Console.ReadLine();

            Console.WriteLine($"Hasil Replace String : {kalimat.Replace(kataLama, kataBaru)}");
        }
        static void removeString() {
            Console.WriteLine("=== RemoveString String ===");
            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();
            Console.Write("Isi parameter remove : ");
            int param = int.Parse(Console.ReadLine());

            Console.WriteLine($"Hasil Remove String : {kalimat.Remove(param)}");
        }
        static void insertString() {
            Console.WriteLine("=== Insert String ===");
            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();
            Console.Write("Isi parameter insert: ");
            int param = int.Parse(Console.ReadLine());

            Console.Write("Masukkan input string : ");
            string input = Console.ReadLine();

            Console.WriteLine($"Hasil Remove String : {kalimat.Insert(param, input)}");
        }
    }
}
