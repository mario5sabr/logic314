﻿using System;

namespace Day03
{
    internal class Program
    {
        static void Main(string[] args)
        {
            stringToCharArray();
        }

        static void tugas3()
        {
            int a, p, h;
            Console.WriteLine("=== TUGAS 3 Modulus ===");
            Console.Write("Angka : ");
            a = int.Parse(Console.ReadLine());
            Console.Write("Pembagi : ");
            p = int.Parse(Console.ReadLine());
            h = a % p;
            if (h == 0)
            {
                Console.WriteLine("Angka " + a + " % " + p + "adalah " + h);
            }
            else
            {
                Console.WriteLine("Angka " + a + "% " + p + "bukan 0 melainkan " + h);
            }
        }

        static void tugas4()
        {
            int r, p, s;
            Console.WriteLine("=== TUGAS 4 ===");
            Console.Write("Input Berapa Puntung Rokok : ");
            p = int.Parse(Console.ReadLine());
            r = 0;
            s = 0;

            if (p >= 20)
            {
                s = p % 8;
                r = p / 8;
                int batangR = r;
                int hargaJ = batangR * 500;

                Console.WriteLine("Hasil rangkai puntung batang rokok sebanyak " + batangR + " dengan sisa hasil puntung rokok " + s);
                Console.WriteLine("Penghasilan dari penjualan rokok : Rp." + hargaJ);
            }
            else if (p >= 8)
            {
                r = p / 8;
                s = p % 8;

                int batangR = r;
                int hargaJ = batangR * 500;

                Console.WriteLine("Hasil rangkai puntung batang rokok sebanyak " + batangR + " dengan sisa hasil puntung rokok " + s);
                Console.WriteLine("Penghasilan dari penjualan rokok : Rp." + hargaJ);
            }
            else
            {
                Console.WriteLine("Sorry, total jumlah puntung rokok kurang dari 8 sehingga tidak dapat dirangkai.");
            }
            Console.ReadLine();

        }
        static void tugas5() {
            int n=100,g;
            Console.WriteLine("=== GRADE NILAI ===");
            Console.Write("Input nilai anda : ");
            n = int.Parse(Console.ReadLine());
            if (n >= 80 && n<=100)
            {
                Console.WriteLine("Asik dape nilai A nih");
            }
            else if (n >= 60 && n < 80)
            {
                Console.WriteLine("Asik dapet nilai B nih");
            }
            else if (n < 0 || n >100) { 
                Console.WriteLine("Angka yang dapat di input 0 - 100");
            }
            else {
                Console.WriteLine("Tidak apa dapet C");

            }
            
        }
        static void tugas6() {
            int a, h;
            Console.Write("Input angka kamoh : ");
            a = int.Parse(Console.ReadLine());
            h = a % 2;
            if (h == 0)
            {
                Console.WriteLine("Angka " + a + " adalah GENAP");
            }
            else {
                Console.WriteLine("Angka "+ a + " adalah GANJIL");
            }
        }
        static void perulanganWhile() {
            Console.WriteLine("=== Perulangan While===");
            bool ulangi = true;
            int nilai = 1;

            while (ulangi) {
                Console.WriteLine($"Proses ke : {nilai}");
                nilai++;

                Console.WriteLine("Ulangi Proses ? (y/n)");
                string input = Console.ReadLine();
                if (input.ToLower() == "y")
                {
                    ulangi = true;
                }
                else if (input == "n")
                {
                    ulangi = false;
                }
                else {
                    nilai = 1;
                    Console.WriteLine("Input yang dimasukkan salah");
                    ulangi = false;
                }
            }
        }
        static void perulanganDoWhile() {
            int a = 0;
            do
            {
                Console.WriteLine("a");
                a++;
            } while (a < 5);
        }
        static void perulanganFor() {
            for (int i = 10; i > 0; i--) { 
            
            }
        }
        static void perulanganBreak() {
            for (int i = 0; i < 10; i++) {
                if (i == 5) {
                    break;
                }
                Console.WriteLine(i);
            }
        }
        static void perulanganContinue() {
            for (int i = 0; i < 10; i++) {
                if (i == 5) {
                    continue;
                }
                Console.WriteLine(i);
            }
        }

        static void forBersarang() {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    Console.Write($"[{i},{j}]");
                }
                Console.WriteLine();
            }
        }
        static void arrVar() {
            Console.WriteLine("ARRAY");
            //inisialisasi array fixed
            int[] staticArray = new int[3];
            //inisialisasi array 1 item setiap menit
            staticArray[0]= 1;
            staticArray[1]= 3;
            staticArray[2] = 5;

            Console.WriteLine(staticArray[0]);
            Console.WriteLine(staticArray[1]); 
            Console.WriteLine(staticArray[2]);

        }
        static void arrForEach()
        {
            //inisialisasi a dynamic item array during deklarasi
            string[] strArray = new string[] {
                "Ilham Rizki",
                "Laudry",
                "Mauva",
                "Mario",
                "Asti"
            };

            foreach (string str in strArray)
            {
                Console.WriteLine(str);
            }
        }
        static void arrFor() {
            string[] strArray = new string[] {
                "Ilham Rizki",
                "Laudry",
                "Mauva",
                "Mario",
                "Asti"
            };

            //baca arra dengan pakai For
            for (int i = 0; i < strArray.Length; i++) {
                Console.WriteLine(strArray[i]);
            }
        }
        static void arr2dimensi()
        {
            int[,] array = new int[,] {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
            };

            //cetak
            Console.WriteLine(array[0, 1]);
            Console.WriteLine(array[1, 2]);
            Console.WriteLine(array[2, 2]);
        }
        static void arr2DimensiFor() { 
            Console.WriteLine("=== Array 2 Dim With FOR===");
            int[,] array = new int[,] {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
            };
            //cetak dengan for
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) { 
                    Console.Write(array[i, j]+" ");
                }
                Console.WriteLine();
            }
        }
        static void splitJoin()
        {
            Console.WriteLine("=== SPLIT JOIN ===");
            Console.Write("Masukkan Kalimat apapun : ");
            string kalimat = Console.ReadLine();
            string[] kataArray = kalimat.Split(" ");

            foreach (string klmt in kataArray) { 
                Console.WriteLine($"{klmt}");
            }
            Console.WriteLine(string.Join(",", kataArray));
        }
        static void subString() {
            Console.WriteLine("=== Sub String ===");
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine();

            Console.WriteLine("Subsstring(1,4) : " + kalimat.Substring(1, 4));
            Console.WriteLine("Subsstring(5,2) : " + kalimat.Substring(5, 2));
            Console.WriteLine("Subsstring(7,9) : " + kalimat.Substring(7, 9));
            Console.WriteLine("Subsstring(9) : " + kalimat.Substring(9));
        }
        static void stringToCharArray() {
            Console.WriteLine("=== String Char Array ===");
            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();

            char[] arr = kalimat.ToCharArray();

            foreach (char klmt in arr) {
                Console.WriteLine(klmt);
            }
        }
    }

}
