﻿using System;
using System.Drawing;

namespace Logic07_PR
{
    internal class Program
    {
        static void Main(string[] args)
        {
            soal10();
        }
        static void soal01()
        {
            Console.WriteLine("=== Anak Faktorial ===");
            Console.Write("Masukkan X : ");
            int x = int.Parse(Console.ReadLine());

            int hasil = 1;
            Console.Write(x + "! = ");
            for (int i = x; i >= 1; i--)
            {
                Console.Write(i);
                hasil *= i;
                if (i > 1)
                {
                    Console.Write("x");
                }
            }
            Console.WriteLine(" = " + hasil);
            Console.WriteLine($"ada {hasil} cara");

        }

        static void soal01_02() {
            Console.WriteLine("=== Anak Faktorial ===");
            Console.Write("Masukkan X : ");
            int x = int.Parse(Console.ReadLine());

            int hasil = 1;
            string angka = "";
            Console.Write(x + "! = ");

            for (int i = x; i >= 1; i--) {
                hasil *= i;
                angka += angka == "" ? i.ToString() : " * " + i.ToString();
            }
            Console.WriteLine($" {angka} = {hasil}");
            Console.WriteLine($"ada {hasil} cara");
        }
        static void soal02()
        {
            Console.WriteLine("=== S O S ===");

            Console.Write("Masukkan string: ");
            string input = Console.ReadLine().ToUpper();

            int count = 0;
            int index = input.IndexOf("SOS");
            int notSos = 0;
            while (index != -1)
            {
                count++;
                index = input.IndexOf("SOS", index + 3);
                notSos = (input.Length - (count * 3)) / 3;
            }

            //Console.WriteLine($"Jumlah kemunculan SOS: {count}");
            Console.WriteLine($"Jumlah muncul bukan SOS : {notSos}");

            string sosString = "";
            for (int i = 0; i < count; i++)
            {
                sosString += "SOS";
                if (i < count - 1)
                {
                    sosString += " ";
                }
            }

            Console.WriteLine(sosString);
        }
        //static void soal02_02() {
        //    char[] sinyal = Console.ReadLine().ToUpper().ToCharArray();

        //    int count = 0;
        //    int countb = 0;

            
        //    if (){
        //        for (int i = 1; ) {
        //            if (sinyal[i] != 'S' || sinyal[i] != 'O' || sinyal[i] != '')
        //            {
        //                Console.WriteLine("Masukkan sinyal yg bener");
        //            }
        //        }
        //    }
        //}


        static void soal03() {
            Console.WriteLine("=== Peminjaman Buku ===");
            Console.Write("Masukkan Tanggal Pinjam Buku (dd/mm/yyyy): ");
            string input = Console.ReadLine();
            DateTime tanggal = DateTime.Parse(input);
            Console.Write("Masukkan Tanggal Pengembalian buku (dd/mm/yyyy): ");
            string input2 = Console.ReadLine();
            DateTime now = DateTime.Parse(input2);
            int denda = 500 * 1;

            TimeSpan span = now - tanggal;

            Console.WriteLine($"Kamu telat bayar sebanyak : {span.Days} hari");

            if (tanggal.Day < tanggal.Day + 3)
            {
                denda = denda * span.Days;
                Console.WriteLine($"Dendannya sekian : {denda}");
            }
            else {
                Console.WriteLine("Pengembalian Sukses tidak ada denda");
            }

        }
        static void soal04()
        {
            Console.WriteLine("=== Hitung FT1 ===");
            Console.Write("Input Tanggal Mulai (dd/mm/yyyy) :");
            DateTime input1 = DateTime.Parse(Console.ReadLine());
            Console.Write("Masukkan tanggal Libur (tgl > 1 masukkan tanda (,)): ");
            string[] input2 = Console.ReadLine().Split(',');
            DateTime[] libur = new DateTime[input2.Length];

            for (int i = 0; i < input2.Length; i++)
            {

                libur[i] = DateTime.ParseExact(input2[i], "dd/MM/yyyy", null);
            }
            DateTime tanggalSkrng = input1;
            int dayCount = 1;
            while (dayCount <= 10)
            {
                if (tanggalSkrng.DayOfWeek != DayOfWeek.Saturday && tanggalSkrng.DayOfWeek != DayOfWeek.Sunday && Array.IndexOf(libur, tanggalSkrng) == -1)
                {
                    dayCount++;
                }
                tanggalSkrng = tanggalSkrng.AddDays(1);
            }
            Console.WriteLine("FT1 kamu adalah tanggal : " + tanggalSkrng.ToString("dd/MM/yyyy"));
            TimeSpan tanggal11 = tanggalSkrng - input1;
            Console.WriteLine("Total hari : " + tanggal11.TotalDays);
        }

        static void soal04_02() {
            Console.Write("Input Tanggal Mulai (dd/mm/yyyy) :");
            DateTime dateStart = DateTime.Parse(Console.ReadLine());
            Console.Write("Masukan berapa lagi akan ujian = ");
            int exam = int.Parse(Console.ReadLine());
            Console.Write("Masukkan tanggal Libur (tgl > 1 masukkan tanda (,)): ");
            int[] holiday = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
            DateTime tanggalSelesai = dateStart.AddDays(-1);
            for (int i = 0; i < exam; i++) {
                tanggalSelesai = tanggalSelesai.AddDays(1);
                if (tanggalSelesai.DayOfWeek == DayOfWeek.Saturday) {
                    tanggalSelesai = tanggalSelesai.AddDays(2);
                }
                for (int j = 0; j < holiday.Length; j++) {
                    if (tanggalSelesai.Day == holiday[j]) {
                        tanggalSelesai = tanggalSelesai.AddDays(1);
                        if (tanggalSelesai.DayOfWeek == DayOfWeek.Saturday) {
                            tanggalSelesai = tanggalSelesai.AddDays(2);
                        }
                    }
                }
            }
            DateTime tanggalUjian = tanggalSelesai.AddDays(1);
            if (tanggalUjian.DayOfWeek == DayOfWeek.Saturday) { 
                tanggalUjian= tanggalUjian.AddDays(2);
            }
            Console.Write("\n");
            Console.WriteLine($"Kelas akan ujian pada tanggal = " + tanggalUjian.ToString("dddd/ dd/MM/yyyy"));
        }

        static void soal05() {
            Console.WriteLine("=== VOKAL VS KONSONAN ===");
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine().ToLower();
            char[] n = kalimat.ToCharArray(); 
            int vokal = 0;
            int konsonan = 0;

            for (int i = 0; i < kalimat.Length; i++) {
                if (n[i] == 'a' || n[i] == 'i' || n[i] == 'u' || n[i] == 'e' || n[i] == 'o')
                {
                    vokal++;
                    
                }
                else if (n[i] != 'a' && n[i] != 'i' && n[i] != 'u' && n[i] != 'e' && n[i] != 'o' && n[i] != ' ') {
                    konsonan++;
                    
                }
            }
            Console.WriteLine("Jumlah huruf \n Vokal : " + vokal);
            Console.WriteLine("Jumlah huruf \n Konsonan : " + konsonan);


        }
        static void soal05_02() {
            Console.WriteLine("=== VOKAL VS KONSONAN ===");
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine().ToLower();
            char[] n = kalimat.ToCharArray();
            int vokal = 0;
            int konsonan = 0;

            for (int i = 0; i < kalimat.Length; i++)
            {
               vokal = (n[i] == 'a' || n[i] == 'i' || n[i] == 'u' || n[i] == 'e' || n[i] == 'o' ? vokal++ : vokal = n[i] == ' ' ? konsonan++ : vokal +=1);





                
            }
            Console.WriteLine("Jumlah huruf \n Vokal : " + vokal);
            Console.WriteLine("Jumlah huruf \n Konsonan : " + konsonan);

        }


        //    Console.Write("Masukkan kalimat: ");
        //    string kalimat = Console.ReadLine().ToLower();
        //    int vokal = 0, konsonan = 0;
        //    foreach (char c in kalimat)
        //    {
        //        if (Char.IsLetter(c))
        //        {
        //            (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' ? ref vokal : ref konsonan) += 1;
        //        }
        //    }
        //    Console.WriteLine("Jumlah huruf vokal: " + vokal);
        //    Console.WriteLine("Jumlah huruf konsonan: " + konsonan);
        //}


    static void soal06() {
            Console.WriteLine("=== Nama + Bintang-bintang ===");
            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine().ToLower().ToString().Replace(" ", "");

            for (int i = 0; i < kalimat.Length; i++) {
                Console.WriteLine($"***{kalimat[i]}***");
            }

        }
        static void soal07() {
            Console.WriteLine("=== Pemesanan Menu Makanan ===");
            Console.Write("Masukkan jumlah makanan : ");
            int jml = int.Parse(Console.ReadLine());
            Console.Write("Masukkan jumlah makanan alergi = ");
            int alergi = int.Parse(Console.ReadLine());
            int bisaDimakan = 0;
            jml = 0;
            int index = 0;
            int uang = 0;
            int sisa = 0;

        ulang:
            Console.Write("Masukkan harga menu = ");
            int[] hargaMenu = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            if (hargaMenu.Length > jml)
            {
                Console.WriteLine("Harga yang dimasukkan berlebih, masukkan kembali");
                goto ulang;
            }
            else {
                Console.Write("Masukkan uang anda = ");
                uang = int.Parse(Console.ReadLine());

                for (int i = 0; i < hargaMenu.Length; i++) {
                    jml += hargaMenu[i];
                }
                bisaDimakan = jml - hargaMenu[alergi];
                index = bisaDimakan / 2;
                sisa = uang - index;

                if (sisa > 0)
                {
                    Console.WriteLine("\nAnda harus membayar = " + index.ToString("Rp #,##"));
                    Console.WriteLine("Sisa uang anda = " + sisa.ToString("Rp #,##"));
                }
                else if (sisa == 0)
                {
                    Console.WriteLine("\nUang anda pas tidak ada kembalian");
                }
                else {
                    Console.WriteLine($"\nUang anda kurang" + sisa.ToString("Rp #,##"));
                }
            }
        }
        static void soal08() {
            Console.WriteLine("=== Bintang Indah ===");
            Console.Write("Masukkan berapa bintang indah yang dimau : ");
            int n = int.Parse(Console.ReadLine());
            
            //n = n != 0 ? n : 0;

            for (int i = 0; i < n; i++) {
                //Mencetak spasi sebanyak n-1

                for (int j = 0; j < n; j++) {
                    if (j < n - 1 - i)
                    {
                        Console.Write(" ");
                    }
                    else
                    {
                        Console.Write("*");
                    }
                } 
                //for (int j = 1; j <= n - i; j++) 
                //{
                //    Console.Write(" ");
                //}

                //for (int j = 1; j <= i; j++) {
                //    Console.Write("*");
                //}
                //pindah ke baris baru setelah mencetaksetiap baris
                Console.WriteLine();
            }

        }
        static void soal09() {
            Console.WriteLine("=== Diagonal Difference ===");

            int[,] matrix = new int[,]{
                { 11, 2, 4},
                { 4, 5, 6},
                { 10, 8, -12}
            };
            int diagonalSum = 0;
            string tampungDiagonal1 = "";

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                diagonalSum += matrix[i, i];
                tampungDiagonal1 += tampungDiagonal1 == "" ? matrix[i,i].ToString() : matrix[i,i] < 0 ? " - " + Math.Abs(matrix[i,i]).ToString() : " + " + matrix[i, i].ToString();
            }

            int keduaDiagonalSum = 0;
            string tampungDiagonal2 = "";
            for (int i = 0; i < matrix.GetLength(0); i++) {
                keduaDiagonalSum += matrix[i, matrix.GetLength(0) - 1 - i];
                tampungDiagonal2 += tampungDiagonal2 == "" ? matrix[i, matrix.GetLength(0) - 1 - i].ToString() : " + " + matrix[i, matrix.GetLength(0) - 1 - i].ToString();
            }
            int diagonalDifference = diagonalSum - keduaDiagonalSum;

            Console.WriteLine($"Diagonal pertama {tampungDiagonal1} = {diagonalSum}");
            Console.WriteLine($"Diagonal kedua {tampungDiagonal2} = {keduaDiagonalSum}");
            Console.WriteLine($"Perbedaan Diagonal {diagonalSum} + {keduaDiagonalSum} = {diagonalDifference}" );
        }
        static void soal10() {
            Console.WriteLine("=== Banyak lilin ditiup ===");
            {
                Console.Write("Masukkan jumlah lilin = ");
                int[] lilin = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
                int max = 0;
                int hitung = 0;

                for (int j = 0; j < lilin.Length; j++) {
                    if (lilin[j] >= max) { 
                        max = lilin[j];
                    }
                    if (lilin[j] == max) {
                        hitung += 1;
                    }
                }
                Console.WriteLine(hitung);

            }
        }
        static void soal11() {
            Console.WriteLine("=== Mengubah posisi index ===");
            Console.Write("Masukkan input angka (Gunakan koma) = ");
            int[] arr = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
            Console.Write("Masukkan rotasi = ");
            int rotasi = int.Parse(Console.ReadLine());

            for (int i = 0; i < rotasi; i++) {
                int tampung = arr[0];
                for (int j = 0; j < arr.Length - 1; j++) {
                    arr[j] = arr[j + 1];
                }
                arr[arr.Length - 1] = tampung;
            }
            Console.Write($"Rotasi {rotasi} = ");
            for (int i = 0; i < arr.Length; i++) {
                Console.Write(arr[i]);
                if (i != arr.Length - 1) {
                    Console.Write(",");
                }
            }
        }
    }
}
