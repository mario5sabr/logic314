﻿using System;

namespace Logic05
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //converArrayAll();
            padleft();
            //contain();
        }
        static void converArrayAll() {
            Console.WriteLine("=== Convert Array All ===");
            Console.Write("Masukkan angka array (Menggunakan Koma) : ");
            int[] array = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            Console.WriteLine(String.Join("\t", array));
            
        }
        static void padleft() {
            Console.WriteLine("=== Pad Left ===");
            Console.Write("Masukkan input kalimat : ");
            int input = int.Parse(Console.ReadLine());
            Console.Write("Masukkan panjang karakter : ");
            int panjang = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Char : ");
            char chars = char.Parse(Console.ReadLine());

            Console.Write($"Hasil PadLeft : {input.ToString().PadLeft(panjang,chars)}");

          
        }
        static void contain() {
            Console.WriteLine("=== Contain ===");
            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();
            Console.Write("Masukkan contain : ");
            string contain = Console.ReadLine();

            if (kalimat.Contains(contain))
            {
                Console.WriteLine($"Kalimat ({kalimat}) ini mengandung {contain}");

            }
            else {
                Console.WriteLine($"Kalimat ({kalimat}) ini tidak mengandung {contain}");
            }
        }

    }
}
