﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Logic06
{
    internal class Program
    {
        static void Main(string[] args)
        {
            listRemove();
            //ListClass();
            //list();
            //contohClass();
            Console.ReadKey();
        }

        static void listRemove() {
            Console.WriteLine("=== List Add ===");
            List<User> listuser = new List<User>() {
                new User (){ Name = "Isni", Age = 22},
                new User (){ Name = "Astika", Age = 23},
                new User (){ Name = "Alfi", Age = 23}


            };
            listuser.Add(new User() { Name = "Anwar", Age = 24 });
            listuser.RemoveAt(2);
            for (int i = 0; i < listuser.Count; i++)
            {
                Console.WriteLine(listuser[i].Name + " sudah berumur " + listuser[i].Age + " Tahun");
            }
        }

            static void listAdd() {
            Console.WriteLine("=== List Add ===");
                List<User> listuser = new List<User>() {
                new User (){ Name = "Isni", Age = 22},
                new User (){ Name = "Astika", Age = 23},
                new User (){ Name = "Alfi", Age = 23}

                
            };
                listuser.Add(new User() { Name = "Anwar", Age = 24 });
                for (int i = 0; i < listuser.Count; i++)
                {
                    Console.WriteLine(listuser[i].Name + " sudah berumur " + listuser[i].Age + " Tahun");
                }
            }

        static void ListClass() {
            List<User> listuser = new List<User>() { 
                new User (){ Name = "Isni", Age = 22},
                new User (){ Name = "Astika", Age = 23},
                new User (){ Name = "Alfi", Age = 23}

            };
            for (int i = 0; i < listuser.Count; i++) {
                Console.WriteLine(listuser[i].Name + " sudah berumur "+ listuser[i].Age + " Tahun");
            }
        }
        static void contohClass() {
            Mobil mobil = new Mobil("RI 1");

            mobil.percepat();
            mobil.maju();
            mobil.isiBensin(12);

            string platno = mobil.getPlatno();

            Console.WriteLine($"Plat Nomor : {platno}");

            Console.WriteLine($"Bensin : {mobil.bensin}");
            Console.WriteLine($"Kecepatan : {mobil.kecepatan}");
            Console.WriteLine($"Posisi : {mobil.posisi}");
        }
        static void list() { 
            Console.WriteLine("=== Listing ===");
            List<string> listOfNames = new List<string>(){ 
                "Astika",
                "marcell",
                "Alwi",
                "Tono"
            };
            Console.WriteLine(String.Join(", ",listOfNames));
        }
    }
}
