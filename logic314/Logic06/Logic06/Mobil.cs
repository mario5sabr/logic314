﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic06
{
    public class Mobil
    {
        public double kecepatan;
        public double bensin;
        public double posisi;
        public string nama;
        public string platno;

        public Mobil() { 
        
        }
        public Mobil(string _platno) { 
            platno = _platno;
        }

        public void percepat() {
            this.kecepatan += 10;
            this.bensin -= 5;

        }

        public void maju() {
            this.posisi += this.kecepatan;
            this.bensin -= 2;
        }

        public void isiBensin(double bensin) {
            this.bensin += bensin;
        }
        public string getPlatno() {
            return platno;
        }
    }
}
