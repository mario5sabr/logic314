﻿using System;

namespace Project_PR2
{
    internal class Program
    {
        static void Main(string[] args)
        {

            soal02();
        }
        static void soal01() {
            Console.WriteLine("=== Tugas 01 ===");
            int golongan, jamKerja;
            double upah, upahLembur, totalUpah;

            Console.Write("Masukkan golongan karyawan (1-4): ");
            golongan = int.Parse(Console.ReadLine());
            Console.Write("Masukkan jumlah jam kerja: ");
            jamKerja = int.Parse(Console.ReadLine());

           
            switch (golongan)
            {
                case 1:
                    upah = 2000;
                    break;
                case 2:
                    upah = 3000;
                    break;
                case 3:
                    upah = 4000;
                    break;
                case 4:
                    upah = 5000;
                    break;
                default:
                    Console.WriteLine("Golongan yang dimasukkan tidak valid.");
                    return;
            }

            if (jamKerja > 40)
            {
                int jamKerjaBiasa = 40;
                int jamLembur = jamKerja - jamKerjaBiasa;
                upahLembur = jamLembur * upah * 1.5;
            }
            else
            {
                upahLembur = 0;
            }

            
            totalUpah = jamKerja * upah + upahLembur;

            
            Console.WriteLine("Upah: " + jamKerja * upah);
            Console.WriteLine("Lembur: " + upahLembur);
            Console.WriteLine("Total: " + totalUpah);
        }
        static void soal02()
        {
            
                Console.WriteLine("=== Tugas NO.2 ===");
                Console.Write("Masukkan Kalimat : ");
                string[] kalimat = Console.ReadLine().Split(" ");

                //string[] kata = kalimat.Split(" ");

                for (int i = 0; i < kalimat.Length; i++)
                {

                    Console.WriteLine($"kata {i + 1}= {kalimat[i]}");
                }

                Console.WriteLine($"Total kata adalah {kalimat.Length}");
            
        }
        static void soal03()
        {
            Console.WriteLine("=== Mengubah String * ===");
            Console.Write("Masukkan kata yang ingin diubah : ");
            string input = Console.ReadLine();

            // Ubah karakter pada indeks ganjil ke '*'
            string output = "";
            string[] words = input.Split(' ');
            for (int i = 0; i < words.Length; i++)
            {
                if (words[i] != "")
                {
                    string word = words[i];
                    string firstChar = word.Substring(0, 1);
                    string lastChar = word.Substring(word.Length - 1);
                    string middleChars = word.Substring(1, word.Length - 2);
                    middleChars = middleChars.Replace(middleChars, new string('*', middleChars.Length));

                    string newWord = firstChar + middleChars + lastChar;

                    output += newWord + " ";
                }
            }

            // Cetak output
            Console.WriteLine(output);
        }
        static void soal04() {
            Console.WriteLine("=== Mengubah String Middle ===");
            Console.Write("Masukkan Kata Yang Akan Diubah : ");
            string input = Console.ReadLine();

            // Ubah karakter pada indeks genap ke '*'
            string output = "";
            string[] words = input.Split(' ');
            for (int i = 0; i < words.Length; i++)
            {
                if (words[i] != "")
                {
                    string word = words[i];
                    string middleChar = "";
                    if (word.Length % 2 == 1)
                    {
                        int middleIndex = word.Length / 2;
                        middleChar = word.Substring(middleIndex, 1);
                    }
                    else
                    {
                        int middleIndex = word.Length / 2 - 1;
                        middleChar = word.Substring(middleIndex, 2);
                    }

                    output += middleChar + " ";
                }
            }

            // Cetak output
            Console.WriteLine(output);
    }
        static void soal05() {
            Console.WriteLine("=== Ubah String Kata ===");
            Console.Write("Masukkan kata : ");
            string kalimat = Console.ReadLine();

            string[] kata = kalimat.Split(' ');
            string output = "";

            for (int i = 0; i < kata.Length; i++)
            {
                if (kata[i].Length >= 3)
                {
                    output += kata[i].Substring(kata[i].Length - 3) + " ";
                }
            }

            Console.WriteLine(output);
        }

        static void soal06() { 
            Console.WriteLine("=== SYULITT ===");
            int N = 7;
            int current = 3;
            for (int i = 0; i < N; i++)
            {
                Console.Write(current);
                if (i < N - 1) {
                    Console.Write(" * ");
                }
                current *= 3;
            }
        }

        static void soal07()
        {
            Console.WriteLine("=== SYULITTTT ===");
            int n = 7;
            string hasil = "";

            for (int i = 1; i <= n; i++)
            {
                int value = i * 5;
                if (i % 2 == 1)
                {
                    value *= -1;
                }
                hasil += value.ToString() + " ";
            }

            Console.WriteLine(hasil);
        }

        static void soal08()
        {
            Console.WriteLine("Input angka : ");
            int n = int.Parse(Console.ReadLine());
            int a = 1, b = 1, c;
            Console.Write(a + "," + b);

            for (int i = 2; i < n; i++)
            {
                c = a + b;
                Console.Write("," + c);
                a = b;
                b = c;
            }

            Console.WriteLine();
        }
        static void soal09() {
            Console.Write("Masukkan waktu (format 12 jam): ");
            string inputTime = Console.ReadLine();

            // mengonversi waktu ke format 24 jam
            DateTime time = DateTime.Parse(inputTime);
            string outputTime = time.ToString("HH:mm:ss");

            Console.WriteLine("Waktu dalam format 24 jam: " + outputTime);
        }

        static void soal09_02() {
            string huruf = "", merek = "";
            Console.WriteLine("=== Kode Harga Baju ===");
            string formatJam = Console.ReadLine().ToUpper();

            string pm = formatJam.Substring(8, 2);
            int jam = int.Parse(formatJam.Substring(0, 5));

            if (jam <= 12 )
            {


            }
        }
        static void soal10() {
            int kode,harga=0;
            string huruf = "", merek ="";
            Console.WriteLine("=== Kode Harga Baju ===");
            Console.Write("Masukkan Kode Baju = ");
            kode = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Kode Ukuran = ");
            huruf = Console.ReadLine();


            switch (kode) {
                case 1:
                    merek = "IMP";
                    break;
                case 2:
                    merek = "Prada";
                    break;
                case 3:
                    merek = "Gucci";
                    break;

                default:
                    Console.WriteLine("Kode yang dimasukkan tidak valid");
                    return;


            }
            if (kode == 1)
            {
                if (huruf == "S" || huruf == "s")
                {
                    harga = 200000;
                }
                else if (huruf == "M" || huruf == "s")
                {
                    harga = 220000;
                }
                else
                {
                    harga = 250000;
                }
            }
            else if (kode == 2)
            {
                if (huruf == "S" || huruf == "s")
                {
                    harga = 150000;
                }
                else if (huruf == "M" || huruf == "s")
                {
                    harga = 160000;
                }
                else
                {
                    harga = 170000;
                }
            }
            else if (kode == 3) {
                if (huruf == "S" || huruf == "s")
                {
                    harga = 200000;
                }
                else if (huruf == "M" || huruf == "s")
                {
                    harga = 200000;
                }
                else
                {
                    harga = 200000;
                }
            }

            Console.WriteLine("Merk Baju = "+merek);
            Console.WriteLine("Harga = " + harga);
        }
        static void soal11() {
            int hasil;
            Console.WriteLine("=== Belanja Lebaran ===");
            Console.Write("Uang Kamoh : ");
            int uang = int.Parse(Console.ReadLine());
            Console.Write("Harga Baju Kamoh (Pakai Koma): ");
            int[] bajuArray = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
            Console.Write("Harga Celana Kamoh (Pakai Koma) : ");
            int[] celanaArray = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            int maxBelanja = 0;
           
            for (int i = 0; i < bajuArray.Length; i++) {
                for (int j = 0; j < celanaArray.Length; j++) { 
                    hasil = bajuArray[i] + celanaArray[j];
                    if (hasil <= uang && hasil >= maxBelanja) {
                        maxBelanja = hasil;
                    } 
                }
            }
            Console.WriteLine($"Belanja sesuai kebutuhan {maxBelanja}");
        }
        static void soal12() {
            Console.WriteLine("=== NOMOR 12 ===");
            Console.Write("Masukkan inputan : ");
            int n = int.Parse(Console.ReadLine());

            int[] array = new int[n];

            for (int i = 0; i < array.Length; i++) {
                array[i] = i + 1;
            }
            for (int j = 0; j < array.Length; j++) {
                for (int k = 0; k < array.Length; k++) {
                    if (j == 0)
                    {
                        Console.Write($"{array[k]} ");

                    }
                    else if (j == array.Length - 1)
                    {
                        Console.Write($"{array[(array.Length - 1) - k]} ");
                    }
                    else {
                        if (k == 0)
                        {
                            Console.Write("* ");
                        }
                        else if (k == array.Length - 1)
                        {
                            Console.Write("* ");
                        }
                        else {
                            Console.Write("  ");
                        }
                    }
                }
                Console.WriteLine();
            }
        }


    }
}
