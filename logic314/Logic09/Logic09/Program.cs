﻿using System;

namespace Logic09
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //no01();
            no02();
            //no03();
        }
        static void no01()
        {
            Console.WriteLine("=== Menghitung jumlah liter bensin ===");
            Console.Write("Masukkan jumlah berapa alamat customer : ");
            int customerTot = int.Parse(Console.ReadLine());
            double cust1 = 2.0;
            double[] jarak = new double[customerTot - 1];
            for (int i = 0; i < customerTot - 1; i++)
            {
                {
                    switch (i)
                    {
                        case 0:
                            jarak[i] = 0.5;
                            break;
                        case 1:
                            jarak[i] = 1.5;
                            break;
                        case 2:
                            jarak[i] = 0.3;
                            break;
                    }
                }
                double totalDistance = 0;
                for (int j = 0; j < customerTot - 1; j++)
                {
                    totalDistance += jarak[j];
                }
                double fuelNeeded = totalDistance / 2.5;
                Console.WriteLine("Total jarak yang ditempuh dari customer 1 ke customer " + customerTot + ": " + totalDistance + " km");
                Console.WriteLine("Bensin yang dibutuhkan: " + fuelNeeded + " liter");

            }
        }
        static void no02()
        {
            int n;
            Console.Write("Masukkan jumlah kue pukis yang ingin dibuat: ");
            n = int.Parse(Console.ReadLine());
            double terigu = (115 * n) / 15;
            double gulaPasir = (190 * n) / 15;
            double susu = (100 * n) / 15;
            Console.WriteLine("Bahan yang dibutuhkan untuk membuat " + n + " kue pukis:");
            Console.WriteLine("Terigu: " + terigu + " gram"); Console.WriteLine("Gula Pasir: " + gulaPasir + " gram");
            Console.WriteLine("Susu: " + susu + " ml");

        }
    }
    
}



//    static void no03() {    
//}

