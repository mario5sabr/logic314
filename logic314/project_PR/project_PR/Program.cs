﻿using System;

namespace project_PR
{
    internal class Program
    {
        static void Main(string[] args)
        {
            soal05();
        }

        static void soal02()
        {
            int i;
            Console.WriteLine("=== Nilai Grade Mahasiswa ===");
            Console.Write("Input Nilai : ");
            i = int.Parse(Console.ReadLine());
            if (i >= 90 && i <= 100)
            {
                Console.WriteLine("Grade A");
            }
            else if (i >= 70 && i <= 89)
            {
                Console.WriteLine("Grade B");
            }
            else if (i >= 50 && i <= 69)
            {
                Console.WriteLine("Grade C");
            }
            else if (i >= 0 && i <= 50)
            {
                Console.WriteLine("Grade E");
            }
            else
            {
                Console.WriteLine("Nilai yang anda masukkan salah!");
            }
        }

        static void soal03()
        {
            int i = 100000, p;
            Console.WriteLine("=== Point Pulsa ===");
            Console.Write("Masukkan Jumlah Pulsa : ");
            i = int.Parse(Console.ReadLine());
            p = 0;
            if (i >= 10000 && i <= 24999)
            {
                p = 80;
            }
            else if (i >= 25000 && i <= 49999)
            {
                p = 200;
            }
            else if (i >= 50000 && i <= 99999)
            {
                p = 800;
            }
            else
            {
                Console.WriteLine("Jumlah pulsa yang dimasukkan tidak ada");
            }
            Console.WriteLine($"Point yang didapat adalah : {p}");
        }

        static void soal04()
        {
            int belanja, jarak;
            Console.WriteLine("=== GrabFood VOUCHER ===");
            Console.Write("Belanja : ");
            belanja = int.Parse(Console.ReadLine());
            Console.Write("Jarak : ");
            jarak = int.Parse(Console.ReadLine());
            string kodePromo = "JKTOVO";
            bool promoTrue = false;

            Console.Write("Masukkan Promo = ");
            string inputKode = Console.ReadLine();
            Console.WriteLine();

            if (inputKode == kodePromo)
            {
                if (belanja >= 30000)
                {
                    promoTrue = true;
                }
                else
                {
                    Console.WriteLine("Minimum belanja untuk promo JKTOVO adalah 30000");
                }
            }
            else if (kodePromo != "JKTOVO")
            {
                Console.WriteLine("Kode promo yang dimasukkan tidak valid");
            }

            int ongkosKirim = 5000 + (jarak - 5) * 1000;
            ongkosKirim = ongkosKirim < 5000 ? 5000 : ongkosKirim;

            double diskon = 0;
            if (promoTrue)
            {
                if (belanja * 0.4 < 30000)
                {
                    diskon = belanja * 0.4;
                }
                else
                {
                    diskon = 30000;
                }
            }
            double totalHarga = belanja - diskon + ongkosKirim;
            Console.WriteLine($"Belanja : {belanja}");
            Console.WriteLine($"Diskon 40% : {diskon}");
            Console.WriteLine($"Ongkir : {ongkosKirim}");
            Console.WriteLine("----------");
            Console.WriteLine($"Total harga : {totalHarga}");
        }

        static void soal05()
        {
            int belanja, ongkir;
            Console.WriteLine("=== Voucher Shopee ===");
            Console.Write("Belanja : ");
            belanja = int.Parse(Console.ReadLine());
            Console.Write("Ongkos Kirim : ");
            ongkir = int.Parse(Console.ReadLine());
            Console.Write("Pilih Voucher (1 - 3) : ");
            int kodePromo;
            int voucher = int.Parse(Console.ReadLine());
            int diskon = 0;
            int ongkirPotong = 0;

            if (voucher == 1 && belanja >= 30000)
            {
                diskon = 5000;
                ongkirPotong = 5000;
            }
            else if (voucher == 2 && belanja >= 50000)
            {
                diskon = 10000;
                ongkirPotong = 10000;
            }
            else if (voucher == 3 && belanja >= 100000)
            {
                diskon = 10000;
                ongkirPotong = 20000;
            }
            else
            {
                Console.WriteLine("Voucher tidak valid atau total belanja anda kurang dari minimal.");
                Console.ReadLine();
                return;
            }
            //hitung total belanja
            int totalBelanja = (belanja + ongkir) - (diskon + ongkirPotong);

            //output data
            Console.WriteLine("Belanja :      " + belanja.ToString("C"));
            Console.WriteLine("Ongkos Kirim :  " + ongkir.ToString("C"));
            Console.WriteLine("Diskon Ongkir :   " + ongkirPotong.ToString("C"));
            Console.WriteLine("Diskon Belanja :   " + diskon.ToString("C"));
            Console.WriteLine("Total Belanja :    " + totalBelanja.ToString("C"));

        }
        static void soal06()
        {
            Console.WriteLine("=== Baby Groomer ===");
            Console.Write("Masukkan nama anda : ");
            string name = Console.ReadLine();
            Console.Write("Tahun berapa anda lahir? : ");
            int born = int.Parse(Console.ReadLine());
            string gen = "";

            if (born >= 1944 && born <= 1964)
            {
                gen = "Baby Groomer";
            }
            else if (born >= 1965 && born <= 1979)
            {
                gen = "Generasi X";
            }
            else if (born >= 1980 && born <= 1994)
            {
                gen = "Generasi Y (Milenials)";
            }
            else if (born >= 1995 && born <= 2015)
            {
                gen = "Generasi Z";
            }
            else {
                gen = "Tidak masuk generasi zaman now!";
            }
            Console.WriteLine("" + name + ", berdasarkan tahun lahir anda tergolong " + gen);

            Console.ReadLine();


        }
        static void soal07() {
            Console.WriteLine("=== Salary Machine ===");
            //Input Data
            Console.Write("Nama : ");
            string nama = Console.ReadLine();
            Console.Write("Tunjangan : ");
            double tunjangan = double.Parse(Console.ReadLine());
            Console.Write("Gapok (Gaji Pokok) : ");
            double gapok = double.Parse(Console.ReadLine());
            Console.Write("Banyak Bulan : ");
            double babul = double.Parse(Console.ReadLine());

            //Hitung pajak
            double pajak = 0;
            //double rumus= gapok+tunjangan;
            double totalGaji = 0;
            double gajiPerBulan = 0;
            double bpjs = 0.03 * (gapok + tunjangan);
            double totalGapokTunjangan = gapok + tunjangan;


            if (totalGapokTunjangan <= 5000000)
            {
                pajak = 0.05 * totalGapokTunjangan;
            }
            else if (totalGapokTunjangan <= 10000000)
            {
                pajak = 0.1 * totalGapokTunjangan;
            }
            else {
                pajak = 0.15 * totalGapokTunjangan;
            }
            gajiPerBulan = (gapok + tunjangan) - (pajak + bpjs);
            totalGaji = (gapok + tunjangan - (pajak + bpjs)) * babul;

            //Output data
            Console.WriteLine("Karyawan atas nama " + nama + " slip gaji sebagai berikut :");
            Console.WriteLine("Pajak                    " + pajak);
            Console.WriteLine("BPJS                     " + bpjs);
            Console.WriteLine("Gaji/bln                 " + gajiPerBulan);
            Console.WriteLine("Total gaji/banyak bulan  " + totalGaji);
            Console.ReadLine();

        }
        static void soal08() {
            Console.WriteLine("=== BMI Calculator ===");
            Console.Write("Masukkan berat badan anda (kg) : ");
            double berat = double.Parse(Console.ReadLine());
            Console.Write("Masukkan tinggi badan anda (cm) : ");
            double tinggi = double.Parse(Console.ReadLine());
            double meter = tinggi / 100;
            double rumus = (berat / (meter * meter));
            string def = "";

            if (rumus <= 18.5)
            {
                def = "Kurus";
            }
            else if (rumus <= 25)
            {
                def = "langsing/sehat";
            }
            else if (rumus > 25) {
                def = "gemuk";
            }
            Console.WriteLine("Nilai BMI anda adalah " + rumus);
            Console.WriteLine("Anda termasuk berbadan " + def);
            Console.ReadLine();
        }
        static void soal09() {
            Console.WriteLine("=== Nilai Hitung ===");
            Console.Write("Masukkan Nilai MTK : ");
            int mtk = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Nilai Fisika : ");
            int fisika = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Nilai Kimia : ");
            int kimia = int.Parse(Console.ReadLine());
            int rata2=(mtk+fisika+kimia)/3;
            if (rata2 >= 50)
            {
                Console.WriteLine("Nilai Rata-rata : " + rata2);
                Console.WriteLine("Selamat");
                Console.WriteLine("Kamu Berhasil");
                Console.WriteLine("Kamu Hebat");
            }
            else if (rata2 < 50) {
                Console.WriteLine("Nilai Rata-rata : " + rata2);
                Console.WriteLine("Maaf");
                Console.WriteLine("Kamu Gagal");
                
            }

        }
    }
}
