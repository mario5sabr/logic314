create table tblPengarang
(
id bigint primary key identity (1,1),
Kd_pengarang varchar(7)not null,
Nama varchar (30) not null,
Alamat varchar (80) not null,
Kota varchar(15) not null,
Kelamin varchar(1)not null
)
select * from tblPengarang
insert into tblPengarang
values
('P0001','Ashadi','Jl.Beo 25','Yogya','P'),
('P0002','Rian','Jl.Solo 123','Yogya','P'),
('P0003','Suwandi','Jl.Semangka 13','Bandung','P'),
('P0004','siti','Jl.Durian 15','Solo','w'),
('P0005','Amir','Jl.Gajah 33','Kudus','P'),
('P0006','Suparman','Jl.Harimau 25','Jakarta','P'),
('P0007','Jaja','Jl.Singa 7','Bandung','P'),
('P0008','Saman','Jl.Naga 12','Yogya','P'),
('P0009','Anwar','Jl.Tidar 6A','Magelang','P'),
('P0010','Fatmawati','Jl.Renjana 4','Bogor','w')

create table tblGaji(
ID bigint primary key identity (1,1),
Kd_Pengarang varchar (7),
Nama varchar (30),
Gaji decimal(18,4)
)
select * from tblGaji

insert into tblGaji 
values 
('P0002','Rian',600000),
('P0005','Amir',700000),
('P0004','Siti',500000),
('P0003','Suwandi',1000000),
('P0010','Fatmawati',600000),
('P0008','Saman',750000)

--1.Hitung dan tampilkan jumlah pengarang dari table pengarang : 
select COUNT(id) Nama from tblPengarang
select COUNT(id) as jumlahPengarang from tblPengarang

--2. Hitunglah berapa jumlah Pengarang Wanita dan Pria
select COUNT(Kelamin)as JumlahGender,Kelamin from tblPengarang group by Kelamin

--3. Tampilkan record kota dan jumlah kotanya dari table tblPengarang
select COUNT(id)as JumlahKota,Kota from tblPengarang group by Kota

--4. Tampilkan record kota diatas 1 kota dari table tblPengarang.
select Kota, COUNT(id) as KotaDiatas_1
from tblPengarang
group by Kota
	having count  (id) > 1 

--5. Tampilkan Kd_Pengarang yang terbesar dan terkecil dari table tblPengarang.
select max (Kd_Pengarang) as Terbesar ,MIN(Kd_Pengarang) as Terendah from tblPengarang
select kd_pengarang from tblPengarang order by Kd_pengarang desc

select top 1 kd_pengarang from tblPengarang order by Kd_pengarang desc, Kd_pengarang asc;

--6. Tampilkan gaji tertinggi dan terendah.
select MAX(Gaji) AS gajiTertinggi, MIN(Gaji) AS gajiTerendah
FROM tblGaji

--7. Tampilkan gaji diatas 600.000
select Gaji as GajiDiatas_600000 from tblGaji where Gaji > 600000

--8 Tampilkan jumlah gaji.
select sum(Gaji) as JumlahGaji from tblGaji

--9. Tampilkan jumlah gaji berdasarkan Kota
SELECT Kota, SUM(Gaji) AS JumlahGaji FROM tblGaji AS gaji 
JOIN tblPengarang AS pengarang ON gaji.Kd_Pengarang = pengarang.Kd_Pengarang 
GROUP BY pengarang.Kota

SELECT pengarang.Kota, gaji.Gaji AS JumlahGaji FROM tblGaji AS gaji 
INNER JOIN tblPengarang AS pengarang ON gaji.Kd_Pengarang = pengarang.Kd_Pengarang

--10. Tampilkan seluruh record pengarang antara P0003-P0006 dari tabel pengarang.
SELECT * FROM tblPengarang
WHERE Kd_Pengarang BETWEEN 'P0003' AND 'P0006'

--11. Tampilkan seluruh data yogya, solo, dan magelang dari tabel pengarang.
SELECT * FROM tblPengarang WHERE Kota = 'Yogya' OR Kota = 'Solo' OR Kota = 'Magelang'
SELECT * FROM tblPengarang WHERE Kota in('Yogya', 'Solo', 'Magelang')

--12. Tampilkan seluruh data yang bukan yogya dari tabel pengarang.
SELECT * FROM tblPengarang WHERE Kota NOT LIKE 'Yogya'


--13. A
SELECT * FROM tblPengarang WHERE Nama LIKE 'A%'


--13. B
SELECT * FROM tblPengarang WHERE Nama LIKE '%i'

--13. C
SELECT * FROM tblPengarang WHERE Nama LIKE '__a%'

--13. D
SELECT * FROM tblPengarang WHERE Nama NOT LIKE '%n'

--14. Tampilkan seluruh data table tblPengarang dan tblGaji dengan Kd_Pengarang yang sama
SELECT pgrg.ID, pgrg.Kd_Pengarang, pgrg.Nama, pgrg.Alamat, pgrg.Kota, pgrg.Kelamin, tblGaji.Gaji FROM tblPengarang pgrg 
JOIN tblGaji ON pgrg.Kd_Pengarang = tblGaji.Kd_Pengarang

--15. Tampilan kota yang memiliki gaji dibawah 1.000.000
SELECT tblPengarang.Kota, tblGaji.Gaji FROM tblGaji 
JOIN tblPengarang ON tblGaji.Kd_Pengarang = tblPengarang.Kd_Pengarang
WHERE tblGaji.Gaji < 1000000

--16. Ubah panjang dari tipe kelamin menjadi 10
ALTER TABLE tblPengarang ALTER COLUMN kelamin varchar(10) NOT NULL
select * from tblPengarang

--17. Tambahkan kolom [Gelar] dengan tipe Varchar (12) pada tabel tblPengarang
ALTER TABLE tblPengarang add[Gelar] varchar(12)
select *from tblPengarang

--18. Ubah alamat dan kota dari Rian di table tblPengarang menjadi, Jl. Cendrawasih 65 dan Pekanbaru
update tblPengarang set Alamat = 'Jl. Cendrawasih 65', Kota = 'Pekanbaru' where Nama = 'Rian'
select *from tblPengarang

--19. Buatlah view untuk attribute Kd_Pengarang, Nama, Kota, Gaji dengan nama vwPengarang

create view vwPengarang as 

select pgrng.Kd_Pengarang, pgrng.Nama, pgrng.Kota, gj.Gaji
FROM tblPengarang pgrng, tblGaji gj
where pgrng.Kd_pengarang = gj.Kd_Pengarang

select * from vwPengarang
