﻿using System;

namespace Day01
{
    internal class Program
    {
        static void Main(string[] args)
        {
            operatorAritmatika();
        }
        static void Konversi() {
            int umur = 19;
            string strUmur = umur.ToString();

            int myInt = 10;
            double myDouble = 5.25;
            bool myBool = false;
            Console.WriteLine("=== Conversi ===");
            Console.WriteLine(strUmur);
            Console.WriteLine(Convert.ToString(myInt)); // conveert int ke string
            Console.WriteLine(Convert.ToDouble(myInt)); // convert int ke double
            Console.WriteLine(Convert.ToInt32(myDouble)); //convert double ke int
            Console.WriteLine(Convert.ToString(myBool)); //convert bool ke string
        }
        static void operatorAritmatika() {
            int mangga, apel, hasil = 8;

            Console.WriteLine("=== Aritmatika ===");
            Console.Write("mangga = ");
            mangga = int.Parse(Console.ReadLine());
            Console.Write("apel = ");
            apel = int.Parse(Console.ReadLine());

            // operasi penjumlahan dengan operator +
            hasil = jumlahBuah(mangga, apel);

            Console.WriteLine("Hasil mangga + appel = {0}", hasil);
        }

        static int jumlahBuah(int mangga, int apel) {
            int hasil = 0;
            hasil = mangga + apel;
            return hasil;
        }
        static void modUlus() {
            int mangga, orang, hasil = 0;
            Console.WriteLine("=== Modulus ===");
            Console.Write("jumlah mangga = ");
            mangga = int.Parse(Console.ReadLine());
            Console.Write("Jumlah orang = ");
            orang = int.Parse(Console.ReadLine());

            hasil = mangga % orang;

            Console.WriteLine($"Hasil mangga % orang = {hasil}");
        }
        static void operatorPenugasan() {
            int mangga = 10;
            int apel = 8;

            mangga = 15;

            Console.WriteLine("mangga = {0}", mangga);
            Console.WriteLine("=== Pertambahan ===");
            apel += mangga;
            Console.WriteLine("apel = {0}", apel);
            apel = 8;

            Console.WriteLine("=== Pengurangan ===");
            mangga = 15;
            apel -= mangga;
            Console.WriteLine("apel = {0}", apel);
            apel = 8;

            Console.WriteLine("=== Perkalian ===");
            mangga = 15;
            apel *= mangga;
            Console.WriteLine("apel = {0}", apel);
            apel = 8;

            Console.WriteLine("=== Pembagian ===");
            mangga = 15;
            apel /= mangga;
            Console.WriteLine("apel = {0}", apel);
            apel = 8;

            Console.WriteLine("=== Modulus ===");
            mangga = 15;
            apel %= mangga;
            Console.WriteLine("apel = {0}", apel);
        }
        static void programPerbandingan() {
            int mangga, apel = 0;

            Console.Write("jumlah mangga = ");
            mangga = int.Parse(Console.ReadLine());
            Console.Write("Jumlah apel = ");
            apel = int.Parse(Console.ReadLine());

            Console.WriteLine("Hasil perbandingan: ");
            Console.WriteLine("mangga > apel : {0}", mangga>apel);
            Console.WriteLine("mangga >= apel : {0}", mangga >= apel);
            Console.WriteLine("mangga < apel : {0}", mangga < apel);
            Console.WriteLine("mangga <= apel : {0}", mangga <= apel);
            Console.WriteLine("mangga == apel : {0}", mangga == apel);
            Console.WriteLine("mangga != apel : {0}", mangga != apel);
        }
        static void boolEan() {
            Console.Write("Enter your age: ");
            int age = int.Parse(Console.ReadLine());
            Console.Write("Password: ");
            string password = Console.ReadLine();

            bool isAdult = age > 18 ? true : false;
            bool isPasswordValid = password == "admin" ? true : false;

            //Logika AND
            if (!isAdult && isPasswordValid)
            {
                Console.WriteLine("Belum Dewasa Password Benar");
            }
            else if (isAdult && !isPasswordValid) {
                Console.WriteLine("Anda sudah dewasa namun password Salah");
            }
            else if (isAdult && isPasswordValid)
            {
                Console.WriteLine("Welcome To The Club");
            }
            else {
                Console.WriteLine("Sorry, try again!");
            }
                }
    }
}
