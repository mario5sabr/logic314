﻿using System;

namespace Logic02
{
    internal class Program
    {
        static void Main(string[] args)
        {
            switchCase();
        }
        static void ifStatement() {
                Console.WriteLine("=== IF STATEMENT ===");
                int x, y;
                Console.Write("Masukkan nilai x : ");
                x = int.Parse(Console.ReadLine());
                Console.Write("Masukkan nilai y : ");
                y = int.Parse(Console.ReadLine());

                if (x > y)
                {
                    Console.WriteLine("Nilai x Lebih besar dari y ");
                }
                if (y > x)
                {
                    Console.WriteLine("Nilai y lebih besar dari x ");
                }
                if (x == y)
                {
                    Console.WriteLine("Nilai x sama dengan Y");
                }
            }
        static void elseStatement()
        {
            Console.WriteLine("=== IF STATEMENT ===");
            int x, y;
            Console.Write("Masukkan nilai x : ");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai y : ");
            y = int.Parse(Console.ReadLine());

            if (x > y)
            {
                Console.WriteLine("Nilai x Lebih besar dari y ");
            }

            else if (x < y)
            {
                Console.WriteLine("Nilai x lebih kecil dari y ");
            }
            else 
            {
                Console.WriteLine("Nilai x sama dengan Y");
            }
        }
        static void nestedIF() {
            Console.WriteLine("=== IF NESTED ===");
            Console.WriteLine("Input nilai");
            int nilai = int.Parse(Console.ReadLine());
            int maxNilai = 100;
            if (nilai >= 70 && nilai <= maxNilai)
            {
                Console.WriteLine("BERHASILLL!!!!!");
                if (nilai == maxNilai)
                {
                    Console.WriteLine("WOW Tidak Mungkin");
                }

            }
            else if (nilai >= 0 && nilai < 70) {
                Console.WriteLine("Gak lulus");
            }
            else
            {
                Console.WriteLine("ERROR");
            }
        }
        static void ternary() {
            Console.WriteLine("=== TERNARY ===");
            int x, y;
            Console.Write("Masukkan nilai x : ");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai y : ");
            y = int.Parse(Console.ReadLine());
            string hasilTernary = x > y ? "X lebih besar dari y" : x < y ? "X lebih kecil dari y"
                : "X sama dengan y";
            Console.WriteLine(hasilTernary);
        }

        static void switchCase() {
            Console.WriteLine("=== SWITCH ===");
            Console.WriteLine("Pilih Buah Apa saja : (APEL, MANGGA, PISANG)");
            string pilihan = Console.ReadLine().ToLower();

            switch (pilihan) {
                case "apel":
                    Console.WriteLine("Anda memilih Apel");
                    break;
                case "mangga":
                    Console.WriteLine("Anda memilih Mangga");
                    break;
                case "pisang":
                    Console.WriteLine("Anda memilih Pisang");
                    break;
                default:
                    Console.WriteLine("Anda memilih buah yang lain");
                    break;
            }
        }
    }
}
