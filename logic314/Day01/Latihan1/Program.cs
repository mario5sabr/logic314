﻿using System;

namespace Latihan1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            tugas1(); 
        }
        static void tugas1() {
            double pi = 3.14159265358979323846;
            Console.WriteLine("=== Tugas 1 ===");
            Console.Write("Masukkan jari-jari : ");
            double r = Convert.ToDouble(Console.ReadLine());

            double luas = pi * r * r;
            double keliling = 2 * pi * r;

            Console.WriteLine("Luas lingkaran: " + luas);
            Console.WriteLine("Keliling lingkaran: " + keliling);

            Console.ReadLine();
        }
        static void tugas2() {
            int s,l,k;
            Console.WriteLine("=== TUGAS 2 ===");
            Console.Write("Masukkan sisi : ");
            s = int.Parse(Console.ReadLine());
            l = s * s;
            k = 4 * s;

            Console.WriteLine("Luas persegi dengan sisi "+ s + " adalah : "+ l);
            Console.WriteLine("Keliling persegi dengan sisi " + s + " adalah : " + k);

            Console.ReadLine();


        }
    }
}
